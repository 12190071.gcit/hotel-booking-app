const express = require("express");
const { Pool } = require('pg');
const cors = require('cors');

const app = express();
app.use(cors());
app.use(express.json());

const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'HotelBookingApp',
    password: 'Faddayx1697',
    port: 5432, // Default PostgreSQL port
});

// Route for user signup
app.post('/signup', (req, res) => {
    const { name, email, password, userType } = req.body;
    const sql = "INSERT INTO login (name, email, password, userType) VALUES ($1, $2, $3, $4)";
    const values = [name, email, password, userType];
    pool.query(sql, values, (err, data) => {
        if (err) {
            return res.json('Error');
        }
        return res.json(data);
    });
});

// Route for user login
app.post('/login', (req, res) => {
    const { email, password } = req.body;
    const sql = "SELECT * FROM login WHERE email = $1 AND password = $2";
    pool.query(sql, [email, password], (err, data) => {
        if (err) {
            return res.json('Error');
        }
        if (data.rows.length > 0) {
            return res.json('Success');
        } else {
            return res.json('Failed');
        }
    });
});

app.listen(8081, () => {
    console.log("Server listening on port 8081");
});
